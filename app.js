var express = require('express'); // Webframework for nodejs, quite powerfull
var Forecast = require('forecast');
var request = require('request');
var mongoClient = require('mongodb').MongoClient;
var forecast = new Forecast({
  service: process.env.FORECAST_SERVICE,
  key: process.env.FORECAST_KEY
});

var dbUri = process.env.MONGODB_URI ||    // in case of using Heroku
          JSON.parse(process.env.VCAP_SERVICES).mongodb[0].credentials.uri || // in case of using swisscom
          "mongodb://localhost:27017/weather"; // otherwise use local db
var googleapis = "https://maps.googleapis.com/maps/api/geocode/json?address=";
var cacheTime = 10; // In seconds
var app = express();

// The database connection will be stored in this variable
var weatherCollection;

app.use(express.static('public'));

app.get('/:city', function(req, res) { // telling nodeJs to get all commands from / into this function
  var city = req.params.city;
  retriveCityData(city, function(cityData) {
    var coordinates = cityData.coordinates;
    weatherCollection.find({
      latitude: coordinates[0],
      longitude: coordinates[1]
    }, {
      sort: [
        ['timestamp', 'desc']
      ],
      limit: 1
    }).toArray(function(err, data) {
      data = data[0]; // take only the first result
      if (err) return console.dir(err);

      if (data == null || currentTimeStamp() - data.timestamp > cacheTime) {
        fetchWeather(coordinates, function(weather) {
          storeNewWeather(weather, function() {
            sendToClient(res, {
              formatted_address: cityData.formatted_address,
              weather: weather
            });
          });
        });
      } else {
        sendToClient(res, {
          formatted_address:cityData.formatted_address,
          weather:data
        });
      }
    });
  });

});

var retriveCityData = function(city, callback) {
  request({
    url: googleapis + city,
    json: true
  }, function(error, response, body) {

    if (!error && response.statusCode === 200) {
      var location = body.results[0].geometry.location;
      callback({
        formatted_address:body.results[0].formatted_address,
        coordinates: [location.lat,location.lng]
      });
    }
  });
}

var fetchWeather = function(coordinates, callback) {
  forecast.get(coordinates, function(err, weather) {
    if (err) return console.dir(err);
    // remove the _id provided by forecast so that there are no conflicts with mongodb
    delete weather._id;
    callback(weather);
  });
}

var storeNewWeather = function(weather, callback) {
  weather.timestamp = currentTimeStamp();
  weatherCollection.insert(weather, function(err, result) {
    if (err) return console.dir(err);
    callback(weather);
  });
}

var sendToClient = function(res, weather) {
  res.send(weather);
}

var currentTimeStamp = function() {
  return Math.floor(new Date() / 1000);
}

var port = process.env.PORT || 3000 // either use the port 3000 or a port which is in the "environment variable" - the cloud will deliver us such a port

// Connect to the DB and start listening
mongoClient.connect(dbUri, function(err, db) {
  weatherCollection = db.collection('weather');
  app.listen(port); // tell nodejs to listen to this port and give response
  console.log('I am ready and listening on %d', port); // write something nice in the console, that the developer sees, it works
});
